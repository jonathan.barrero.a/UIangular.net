﻿personaServices.factory('personaService', [
    function () {
        return {
            GetPersonas: function () {
                var result = null;
                $.ajax({
                    type: "GET",
                    url: "/Persona/GetPersonas/",
                    contentType: "aplication/json;charset=utf-8",
                    data: "{}",
                    async: false,
                    dataType: "json",
                    success: function (data) {
                        result = data;
                    },
                    error: function (errordata) {
                        alert(errordata.responseText);
                    }
                });

                return result;
            }
        }
    }
]);