﻿var PersonasApp = angular.module('AppPersonas', [
    'personaController',
    'personaServices'
]);

var personaController = angular.module('personaController', []);

var personaServices = angular.module('personaServices', []);